# logging-on

Turn on logging for all modules.

This is meant to be run with the command `plantctl patch logging-on`. This is the
equivalent of cloning this repository and executing
`ansible-playbook -i hosts patch.yml`.